# Theme Style

1. Move the mouse over the `Extension Theme Style icon`.

    ![](/assets/images/ThemeStyle/style_btn.png)

2. Click the `Extension Theme Style icon`.

    ![](/assets/images/ThemeStyle/style_cn.png)

3. Enable different theme styles according to your needs.

    ![](/assets/images/demo.custom_theme.gif)

4. If you want to develop style themes for this extension, please refer to: [https://github.com/Physton/sd-webui-prompt-all-in-one/tree/main/styles/extensions#english](https://github.com/Physton/sd-webui-prompt-all-in-one/tree/main/styles/extensions#english)