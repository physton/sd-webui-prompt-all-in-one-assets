<!-- _navbar.md -->

* [:us:English](/Installation.md#installation)
* [:ru:Русский](/ru/Installation.md#установка)
* [:cn:简体中文](/zh-cn/Installation.md#安装)
* [:cn:繁體中文](/zh-tw/Installation.md#安裝)
* [:coffee:donate️/捐赠](/Donate.md#buy-me-a-coffee-请我喝杯咖啡)
