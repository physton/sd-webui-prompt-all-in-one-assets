# Темы оформления

1. Наведите курсор на значок `Расширенные стили темы`.

   ![](../assets/images/ThemeStyle/style_btn.png)

2. Нажмите на значок `Расширенные стили темы`.

   ![](../assets/images/ThemeStyle/style_en.png)

3. В зависимости от своих потребностей выберите разные темы оформления.

   ![](../assets/images/demo.custom_theme.gif)

4. Если вам нужно разработать тему оформления для этого расширения, см. [https://github.com/Physton/sd-webui-prompt-all-in-one/tree/main/styles/extensions#english](https://github.com/Physton/sd-webui-prompt-all-in-one/tree/main/styles/extensions#english)