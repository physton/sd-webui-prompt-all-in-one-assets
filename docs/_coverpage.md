<!-- _coverpage.md -->

# sd-webui-prompt-all-in-one

> by: Physton

sd-webui-prompt-all-in-one is an extension based on [stable-diffusion-webui](https://github.com/AUTOMATIC1111/stable-diffusion-webui) that aims to improve the user experience of the prompt/negative prompt input box. It has a more intuitive and powerful input interface, provides automatic translation, history and collection functions, and supports multiple languages to meet the needs of different users.

sd-webui-prompt-all-in-one 是一个基于 [stable-diffusion-webui](https://github.com/AUTOMATIC1111/stable-diffusion-webui) 的扩展，旨在提高提示词/反向提示词输入框的使用体验。它拥有更直观、强大的输入界面功能，它提供了自动翻译、历史记录和收藏等功能，它支持多种语言，满足不同用户的需求。

[GitHub](https://github.com/Physton/sd-webui-prompt-all-in-one)
[🇨🇳Gitee](https://gitee.com/physton/sd-webui-prompt-all-in-one)
[Document / 文档](Installation.md#installation)