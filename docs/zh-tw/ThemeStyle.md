# 主題風格

1. 把滑鼠移動到 `擴展主題樣式圖示` 上。

   ![](../assets/images/ThemeStyle/style_btn.png)

2. 點擊 `擴展主題樣式圖示`。

   ![](../assets/images/ThemeStyle/style_cn.png)

3. 根據個人需求，啟用不同的主題風格。

   ![](../assets/images/demo.custom_theme.gif)

4.如果你需要開發此擴展的樣式主題，請參考：[https://github.com/Physton/sd-webui-prompt-all-in-one/tree/main/styles/extensions#简体中文](https://github.com/Physton/sd-webui-prompt-all-in-one/tree/main/styles/extensions#简体中文)