> This project is developed in Physton. The documentation in multiple languages is translated using machine translation. Please excuse any translation errors.
> 
> If you find this extension helpful, please give me a star on Github!
You could also buy me a coffee: [donate](/Donate.md#buy-me-a-coffee-请我喝杯咖啡)
>
> 此项目由 Physton 开发。多国语言文档来自机器翻译，如有翻译错误请谅解。
>
> 如果你觉得这个扩展有帮助，请在Github上给我一颗星！
> 你也可以请我喝杯咖啡: [donate](/Donate.md#buy-me-a-coffee-请我喝杯咖啡)
>
> Telegram: [https://t.me/promptAIO](https://t.me/promptAIO)
>
> QQ群：820700336
> 
> physton@163.com