# Extension update description

1. All your data, including API keys, history records, and favorite data, are stored in the `storage` directory of the extension.

2. Using the built-in updater in WebUI or updating via git will not cause your data to be lost.

3. Overwriting the extension installation with a compressed file or deleting the extension and reinstalling it will result in data loss.

4. If you want to backup your data, you can copy the `storage` directory to another location and then copy it back when needed.