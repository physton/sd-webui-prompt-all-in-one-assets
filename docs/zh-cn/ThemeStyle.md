# 主题风格

1. 鼠标移动到 `扩展主题样式图标` 上。

   ![](../assets/images/ThemeStyle/style_btn.png)

2. 点击 `扩展主题样式图标`。

   ![](../assets/images/ThemeStyle/style_cn.png)

3. 根据自己的需求，开启不同的主题风格。

   ![](../assets/images/demo.custom_theme.gif)

4. 如果你需要开发此扩展的样式主题，请参考：[https://github.com/Physton/sd-webui-prompt-all-in-one/tree/main/styles/extensions#简体中文](https://github.com/Physton/sd-webui-prompt-all-in-one/tree/main/styles/extensions#简体中文)